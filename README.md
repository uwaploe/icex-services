# Docker Services For ICEX

This repository contains a set of [Docker
Compose](https://docs.docker.com/compose/compose-file/) files for
ICEX-18. Each file provides the configuration for a multi-container
application (project).

Each application is started by running the following command in the
project directory:

``` shellsession
$ docker-compose up -d --no-build
```

Stop the application with:

``` shellsession
$ docker-compose stop
```

## gics

Runs all of the GICS services to perform a range survey and monitor the
grid.

## ui

Web services to provide a user interface for the survey and for converting
between grid coordinates and latitude-longitude.

## detection

Services to transfer data from the legacy and new detection systems to
Seascape.

## simdirs

Services to simulate output from the legacy detection system (DIRS) for
testing Seascape.

## gpssim

Services to simulate output from the GPS and RF-GPS units to test the
range survey procedure.

## rfgpstest

Use to test the RF-GPS sampling process. This application should be
started with the `run.sh` shell script as it starts an interactive session
to monitor the GPS messages, exit with ctrl-c:

``` shellsession
$ ./run.sh
Starting rfgpstest_redis_1 ...
Starting rfgpstest_redis_1 ... done
Starting rfgpstest_gps_1 ...
Starting rfgpstest_gps_1 ... done
subscribe
data.refs
1
message
data.refs
{"points":{"h1":{"latitude":731282630,"longitude":-1494154500,"id":"h1","tsec":1515354822},"h2":{"latitude":731289750,"longitude":-1494318780,"id":"h2","tsec":1515354822},"h3":{"latitude":731230570,"longitude":-1494312880,"id":"h3","tsec":1515354823},"h4":{"latitude":731240670,"longitude":-1494086770,"id":"h4","tsec":1515354825},"origin":{"latitude":731265980,"longitude":-1494204700,"id":"origin","tsec":1515354822}}}
message
data.refs
{"points":{"h1":{"latitude":731282630,"longitude":-1494154500,"id":"h1","tsec":1515354832},"h2":{"latitude":731289750,"longitude":-1494318780,"id":"h2","tsec":1515354832},"h3":{"latitude":731230570,"longitude":-1494312880,"id":"h3","tsec":1515354833},"h4":{"latitude":731240670,"longitude":-1494086780,"id":"h4","tsec":1515354835},"origin":{"latitude":731265980,"longitude":-1494204729,"id":"origin","tsec":1515354832}}}
message
data.refs
{"points":{"h1":{"latitude":731282630,"longitude":-1494154500,"id":"h1","tsec":1515354842},"h2":{"latitude":731289750,"longitude":-1494318780,"id":"h2","tsec":1515354842},"h3":{"latitude":731230570,"longitude":-1494312870,"id":"h3","tsec":1515354843},"h4":{"latitude":731240670,"longitude":-1494086770,"id":"h4","tsec":1515354845},"origin":{"latitude":731265980,"longitude":-1494204770,"id":"origin","tsec":1515354842}}}
^CExiting ...
Stopping rfgpstest_gps_1   ... done
Stopping rfgpstest_redis_1 ... done
$
```