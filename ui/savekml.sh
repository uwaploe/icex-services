#!/usr/bin/env bash
#
# Download a KML file containing the current Camp location
#

: ${KMLURL="http://localhost:8082/kml"}
: ${KMLDIR="/data/ICEX/KML"}

mkdir -p $KMLDIR
kmlfile="$KMLDIR/ice_camp_$(date -u +%FT%T).kmz"

poifile="$1"
if [[ -n "$poifile" ]]; then
    curl -s -H "Content-Type: application/json" \
     -X POST $KMLURL \
     -d "@${poifile}" > "$kmlfile"
else
    curl -s -X GET $KMLURL > "$kmlfile"
fi