#!/usr/bin/env bash

docker-compose up -d --no-build
read -p "Press ENTER when ready ..." foo
curl -H "Content-Type: application/json" \
     -X POST http://localhost:8082/kml \
     -d @poi.json > test.kmz
docker-compose stop
docker-compose rm -f
