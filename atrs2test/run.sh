#!/usr/bin/env bash

d="${PWD##*/}"

docker-compose up -d --no-build
trap "docker-compose stop -t 2;exit 0" EXIT HUP INT QUIT TERM
docker exec -it ${d}_redis_1 redis-cli --raw subscribe data.atrs2
echo "Exiting ..."
